import pytest
import logging
from brownie import Wei, reverts, chain, web3
from web3 import Web3
from eth_abi import encode_single
from eth_account.messages import encode_defunct
from pprint import pformat




LOGGER = logging.getLogger(__name__)
POOL_AMMOUNTS = [0 ,20 ,30 ]
zero_address = '0x0000000000000000000000000000000000000000'
fake_address = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
key_chain = 5

key_token_id = 1

#call_amount = 1e18
#eth_amount = "1 ether"
in_type = 0
out_type = 3
ORACLE_ADDRESS = '0x8125F522a712F4aD849E6c7312ba8263bEBeEFeD' 
ORACLE_PRIVATE_KEY = '0x222ead82a51f24a79887aae17052718249295530f8153c73bf1f257a9ca664af'
secret = 777
hashed_secret = Web3.solidityKeccak(['uint256'], [secret])

#use NFT key ERC721
#using wrong secret

def test_simple_wrap(accounts, trustedEncoder, wnft721, dai, NFTkey):
    # Init  - set wnft
    trustedEncoder.setWNFTId(out_type, wnft721.address, 0, {'from':accounts[0]})

    # Init  - set trusted signer
    trustedEncoder.setSignerStatus(ORACLE_ADDRESS, True, {'from':accounts[0]})

    #Init - mint NFT key
    NFTkey.mintWithURI(accounts[8], 1, 'xxxx', {"from": accounts[8]})
        
    # Prepare collateral tokens
    POOL_STORAGES = [accounts[1], accounts[2], accounts[3]]
    for a in POOL_STORAGES:
        dai.transfer(a, POOL_AMMOUNTS[POOL_STORAGES.index(a)], {'from':accounts[0]})
        dai.approve(trustedEncoder, POOL_AMMOUNTS[POOL_STORAGES.index(a)], {'from': a})
        assert dai.balanceOf(a) == POOL_AMMOUNTS[POOL_STORAGES.index(a)]
        assert dai.allowance(a, trustedEncoder) == POOL_AMMOUNTS[POOL_STORAGES.index(a)]

    key_address = NFTkey.address # Fake address for this test
    # Wrap
    # 1. Prepare params
    in_data =( 
        ((in_type, zero_address), 0, 0), # Empty inAsset
        zero_address, # unWrapDestination
        [], # Fees[]
        [
          (
            0x03,  # Lock type
            Web3.solidityKeccak(
                ['uint256','address','uint256','bytes32'], 
                [key_chain, key_address, key_token_id, hashed_secret] # Encoded key info
            ) 
          )
        ], # Locks[]
        [], # royalties
        out_type, # outType 
        0, # outBalance
        0x00 # rules
    )
    collateral = [] #Empty array because encoded
    wrap_for = accounts[9]
    
    # # web3 version 6 
    # encoded_liquidity = (
    #     Web3.solidity_keccak(['address'], [dai.address]),
    #     [
    #         (Web3.solidity_keccak(['address'], [POOL_STORAGES[0].address]), POOL_AMMOUNTS[0]),
    #         (Web3.solidity_keccak(['address'], [POOL_STORAGES[1].address]), POOL_AMMOUNTS[1]),
    #         (Web3.solidity_keccak(['address'], [POOL_STORAGES[2].address]), POOL_AMMOUNTS[2]),
    #     ]
    # )
    
    # web3 version 5 
    encoded_liquidity = (
        Web3.solidityKeccak(['address', 'bytes32'], [dai.address, hashed_secret]),
        [
            (Web3.solidityKeccak(['address', 'bytes32'], [POOL_STORAGES[0].address, hashed_secret]), POOL_AMMOUNTS[0]),
            (Web3.solidityKeccak(['address', 'bytes32'], [POOL_STORAGES[1].address, hashed_secret]), POOL_AMMOUNTS[1]),
            (Web3.solidityKeccak(['address', 'bytes32'], [POOL_STORAGES[2].address, hashed_secret]), POOL_AMMOUNTS[2]),
        ]
    )
    logging.info(pformat(in_data))
    logging.info('-------')
    logging.info(pformat(encoded_liquidity))
    
    tx_wrap = trustedEncoder.wrapEncoded(
        in_data,
        collateral,
        wrap_for,
        encoded_liquidity,
        {'from':accounts[0]}
    )
    logging.info(pformat(tx_wrap.events))



def test_simple_unwrap(accounts, trustedEncoder, wnft721, dai, NFTkey):
    POOL_STORAGES = [accounts[1], accounts[2], accounts[3]]
    wnft = trustedEncoder.getWrappedToken(wnft721, 1, dai, hashed_secret)
    logging.info(pformat(wnft))

    encoded_liquidity = trustedEncoder.getLiquidity(wnft721,1)
    logging.info(pformat(encoded_liquidity))

    logging.info(trustedEncoder.hlpAssetAndSecretHash(dai, hashed_secret))

    proofed_liq_amount = trustedEncoder.getLiquidityProof(
        wnft721, 1, dai,
        POOL_STORAGES,
        hashed_secret
    )
    logging.info('Proofed liquidity: {} \n'.format(proofed_liq_amount))

    for a in POOL_STORAGES:
        logging.info(trustedEncoder.hlpAssetAndSecretHash(a, hashed_secret))
    key_address = NFTkey.address  # Fake address for this test
    
    # Prepare signature
    encoded_msg = encode_single(
         '(address,uint256,address,uint256)',
         ( accounts[9].address,
           key_chain,
           key_address,
           Web3.toInt(key_token_id),
         )
    ) 
    hashed_msg = Web3.solidityKeccak(['bytes32'], [encoded_msg])
    message = encode_defunct(primitive=hashed_msg)
    signed_message = web3.eth.account.sign_message(message, private_key=ORACLE_PRIVATE_KEY)

    logging.info('sign_message is {}'.format(signed_message))

    signature = signed_message.signature

    with reverts("Unexpected proofLock param"):
        tx_unwrap = trustedEncoder.unWrap(
            wnft721.address,
            1,
            dai,
            POOL_STORAGES,
            secret+1, #wrong secret
            signature,
            key_chain,
            key_address,
            key_token_id,
            accounts[9],
            False,
            {'from':accounts[9]}
        )

    with reverts("Unexpected proofLock param"):
        tx_unwrap = trustedEncoder.unWrap(
            wnft721.address,
            1,
            dai,
            POOL_STORAGES,
            secret,
            signature,
            key_chain,
            key_address,
            key_token_id+1, #wrong tokenID of nft key
            accounts[9],
            False,
            {'from':accounts[9]}
        )

