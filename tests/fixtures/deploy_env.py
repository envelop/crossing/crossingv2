import pytest
#from brownie import chain

############ Mocks ########################
@pytest.fixture(scope="module")
def usdt(accounts, MockToken):
    erc = accounts[0].deploy(MockToken, 'USDT Mock Token', 'USDT', 6)
    yield erc

@pytest.fixture(scope="module")
def usdc(accounts, MockToken):
    erc = accounts[0].deploy(MockToken, 'USDC Mock Token', 'USDC', 12)
    yield erc

@pytest.fixture(scope="module")
def dai(accounts, MockToken):
    erc = accounts[0].deploy(MockToken, 'DAI Mock Token', 'DAI', 18)
    yield erc

@pytest.fixture(scope="module")
def NFTkey(accounts, Token721Mock):
    """
    NFT 721 with URI
    """
    t = accounts[0].deploy(Token721Mock, "NFT key for crossing", "NFK")
    t.setURI(0, 'https://maxsiz.github.io/1/')
    yield t  





#######################light version of protocol########################3
@pytest.fixture(scope="module")
def trustedEncoder(accounts, TrustedWrapperEncoder):
    w = accounts[0].deploy(TrustedWrapperEncoder, accounts[0])
    yield w

@pytest.fixture(scope="module")
def wnft721(accounts, Wnft721, trustedEncoder):
    wnft = accounts[0].deploy(Wnft721,"Envelop wNFT", "wNFT", "https://api.envelop.is/metadata/", trustedEncoder.address )
    yield wnft








