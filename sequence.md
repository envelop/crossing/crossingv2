```mermaid
sequenceDiagram
    autonumber
    actor U as User
    box Chain 1
        participant E as Escrow
    end
    box Oracle
        participant O as EnvelopOracle
    end
%%    box LightGreen Oracle & Client
    box Chain 2
        participant P as PoolFactory
        
    end
    actor P1 as Provider_1
    actor P2 as Provider_2
    activate U
    U->>E: Deposit
    E->>U: txHash
    U->>O: RegisterDeposit
    activate O
    O->>O: getAndSignProof
    O-->>U: DepositProof  
    deactivate O
    U->>P: createPool(asset, amount, pinHash, depositProof)
    note over E: Deposit proof include chain, address, asset, amount
    P->>O: poolRegistered
    P1->>P: poolDeposit
    P2->>P: poolDeposit
    P2->>O: getPoolFillProof
    activate O
    O->>O: getAndSignProof
    note left of O: Oracle geting on-chain info
    O-->>P2: proofOfpoolFill
    deactivate O
    P2->>E: ConfirmPoolComplet(merkleRoot)
    activate E
    E->>E: mintKeyNFTForUser
    note left of E: NFT key minted to user
    note left of E: Escrow pool available for providers and closed for users
    deactivate E
    rect rgb(70, 180, 10)
    par ClaimFromEscrow
      P1->>E: claimWithMerkleProof
    and
      P2->>E: claimWithMerkleProof
    end
    end
    alt Transfer key bitween user
    U-->>U: NFT Transfers
    end
    U->>E: burnKey
    U->>O: getBurnProof
    activate O
    O->>O: getAndSignProof1
    O-->>U: keyBurnProof
    deactivate O
    U->>P: createwNFT(proofOfBurn)
    note over P: wNFT belonging to User(pin Locked)
    U->>P: unWrap(pin, forAddress)
    activate P
    P-->>U: funds
    deactivate P

```