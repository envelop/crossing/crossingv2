// SPDX-License-Identifier: MIT
// Crossing solution
pragma solidity 0.8.19;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../interfaces/ITrustedWrapperEncoder.sol";

/// @title WNFT (erc721)  contract
/// @author Crossing Team
/// @notice You can use this contract with main wrapper contracts
/// @dev !!! This contract is not enumerable
/// @custom:please see Docs 
contract Wnft721  is ERC721{
    using Strings for uint256;
    using Strings for uint160;
    
    address public wrapper;
    string  public baseurl;
    
    constructor(
        string memory name_,
        string memory symbol_,
        string memory _baseurl,
        address _wrapper
    ) 
        ERC721(name_, symbol_)  
    {
        wrapper = _wrapper;
        baseurl = string(
            abi.encodePacked(
                _baseurl,
                block.chainid.toString(),
                "/",
                uint160(address(this)).toHexString(),
                "/"
            )
        );

    }

    function mint(address _to, uint256 _tokenId) external {
        require(wrapper == msg.sender, "Trusted address only");
        _mint(_to, _tokenId);
    }

    /**
     * @dev Burns `tokenId`. See {ERC721-_burn}.
     *
     * Requirements:
     *
     * - The caller must own `tokenId` or be an approved operator.
     */
    function burn(uint256 tokenId) public virtual {
        //solhint-disable-next-line max-line-length
        require(wrapper == msg.sender, "Trusted address only");
        //require(_isApprovedOrOwner(_msgSender(), tokenId), "ERC721Burnable: caller is not owner nor approved");
        _burn(tokenId);
    }

    

    // function wnftInfo(uint256 tokenId) external view returns (ETypes.WNFT memory) {
    //     return IWrapperEncoded(wrapper).getWrappedToken(address(this), tokenId);
    // }
    
    
    function baseURI() external view  returns (string memory) {
        return _baseURI();
    }

    function _baseURI() internal view  override returns (string memory) {
        return baseurl;
    }

    /**
     * @dev Function returns tokenURI of **underline original token** 
     *
     * @param _tokenId id of protocol token (new wrapped token)
     */
    function tokenURI(uint256 _tokenId) public view override returns (string memory _uri) {
        _uri = ITrustedWrapperEncoder(wrapper).getOriginalURI(address(this), _tokenId);
        if (bytes(_uri).length == 0) {
            _uri = ERC721.tokenURI(_tokenId);
        }
        return _uri;
    }

    function exists(uint256 _tokenId) public view returns(bool) {
        return _exists(_tokenId);
    }

}
