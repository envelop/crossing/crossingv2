// SPDX-License-Identifier: MIT
// Crossing Solution with wNFT. 

//import "@envelop-protocol-v1/contracts/TokenServiceExtended.sol";
import "@envelop-protocol-v1/contracts/WrapperLightV1.sol";
//import "../interfaces/ITrustedWrapperEncoder.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";


pragma solidity 0.8.19;

contract TrustedWrapperEncoder is WrapperLightV1 {
    using ECDSA for bytes32;
    using SafeERC20 for IERC20Extended;

    struct hashedShare {
        bytes32 hashedSource;
        uint256 amount;
    }

    struct BlurredLiq {
        bytes32 hashedAsset;
        hashedShare[] assetShares;
    }

     // Oracle signers status
    mapping(address => bool) public oracleSigners;

	mapping(address => bool)  public trustedOperator;

    // Map from wrapped token address and id => wNFT to blurred liquidity 
    mapping(address => mapping(uint256 => BlurredLiq)) public blurred; 

    constructor (address _trusted)
    {
    	trustedOperator[_trusted] = true;
    } 

	modifier onlyTrusted() {
        require (trustedOperator[msg.sender], "Only trusted address");
        _;
    }


    function wrapEncoded(
        ETypes.INData calldata _inData, 
        ETypes.AssetItem[] calldata _collateral, 
        address _wrappFor,
        BlurredLiq calldata _liquidity
    ) 
        public
        virtual 
        payable
        onlyTrusted 
        nonReentrant 
        returns (ETypes.AssetItem memory) 
    {
        

        // 1. Mint wNFT
        _mintNFT(
            _inData.outType,     // what will be minted instead of wrapping asset
            lastWNFTId[_inData.outType].contractAddress, // wNFT contract address
            _wrappFor,                                   // wNFT receiver (1st owner) 
            lastWNFTId[_inData.outType].tokenId + 1,        
            _inData.outBalance                           // wNFT tokenId
        );
        lastWNFTId[_inData.outType].tokenId += 1;  //Save just minted id 

        // TODOb  Think about remove/override this internal
        // 4. Safe wNFT info
        _saveWNFTinfo(
            lastWNFTId[_inData.outType].contractAddress, 
            lastWNFTId[_inData.outType].tokenId,
            _inData
        );

        // Safe info about external blurred liquidity
        blurred
            [lastWNFTId[_inData.outType].contractAddress]
            [lastWNFTId[_inData.outType].tokenId] 
        = _liquidity;

        

        emit WrappedV1(
            _inData.inAsset.asset.contractAddress,        // inAssetAddress
            lastWNFTId[_inData.outType].contractAddress,  // outAssetAddress
            _inData.inAsset.tokenId,                      // inAssetTokenId 
            lastWNFTId[_inData.outType].tokenId,          // outTokenId 
            _wrappFor,                                    // wnftFirstOwner
            msg.value,                                    // nativeCollateralAmount
            _inData.rules                                 // rules
        );
        return ETypes.AssetItem(
            ETypes.Asset(_inData.outType, lastWNFTId[_inData.outType].contractAddress),
            lastWNFTId[_inData.outType].tokenId,
            _inData.outBalance
        );
    }


    function unWrap(
        address _wNFTAddress, 
        uint256 _wNFTTokenId,
        address _liquidityAsset,
        address[] memory _sources,
        uint256 _secret,
        bytes calldata _signature,
        uint256 proofChain,
        address proofContract,
        uint256 proofTokenId,
        address unwrapFor,
        bool _isEmergency
    ) external virtual {

        
        BlurredLiq memory liq = blurred[_wNFTAddress][_wNFTTokenId]; 
       
        { // because Stack Too Deep warning
            _checkCoreUnwrap(ETypes.AssetType.ERC721, _wNFTAddress, _wNFTTokenId);
            ETypes.WNFT memory wnft = getWrappedToken(_wNFTAddress, _wNFTTokenId);    
            // Check Burn Lock: this wNFT can be unwrap only if appropriate
            // nft will be burn  (only white listed)
            // 0x00 - TimeLock
            // 0x01 - TransferFeeLock -  NOT used in Ligth
            // 0x02 - Personal Collateral count Lock check
            // 0x03 - Burn Lock with signature
            for (uint256 i = 0; i < wnft.locks.length; ++ i){
                if (wnft.locks[i].lockType == 0x03 ){
                    require(
                        wnft.locks[i].param == uint256(keccak256(abi.encodePacked(
                            proofChain,
                            proofContract,
                            proofTokenId,
                            keccak256(abi.encodePacked(_secret))
                        ))),
                        "Unexpected proofLock param"
                    );
                }
            }

            // Check oracle proof signature
            bytes32 msgMustWasSigned = keccak256(abi.encode(
                    msg.sender,
                    proofChain,
                    proofContract,
                    proofTokenId
            )).toEthSignedMessageHash();
            require(oracleSigners[msgMustWasSigned.recover(_signature)], "Unexpected signer");

            // Check collateral shares address and total amount
            if (!_isEmergency){
                require(
                    _getBlurredAmount(liq) <= _getProofedBlurredAmount(
                        liq,
                        _liquidityAsset,
                        _sources,
                        keccak256(abi.encodePacked(_secret))
                    ),
                    "Insufficient amount"
                );
            }
        }

        // transfer collateral and burn(?) wnft
        for (uint256 i = 0; i < liq.assetShares.length; i ++) {
            IERC20Extended(_liquidityAsset).safeTransferFrom(_sources[i], unwrapFor, liq.assetShares[i].amount);
        }
        IERC721Mintable(_wNFTAddress).burn(_wNFTTokenId);
    }

    function unWrap(
        ETypes.AssetType _wNFTType, 
        address _wNFTAddress, 
        uint256 _wNFTTokenId, 
        bool _isEmergency
    ) public virtual override {}

    ////////////////////////////////////////
    //     Admin functions               ///
    ////////////////////////////////////////

    function setMaxCollateralSlots(uint256 _count) external onlyOwner {
        MAX_COLLATERAL_SLOTS = _count;
    }

    function setTrustedOperatorStatus(address _operator, bool _status) 
        external onlyOwner
    {
        trustedOperator[_operator] = _status;
    }

    function setSignerStatus(address _signer, bool _status) external onlyOwner {
        oracleSigners[_signer] = _status;
    }
    /////////////////////////////////////////

    function getWrappedToken(
        address _wNFTAddress, 
        uint256 _wNFTTokenId,
        address _liquidityAsset,
        //address[] sources,
        bytes32 _secretHash
    ) 
        public 
        view 
        returns (ETypes.WNFT memory wnft) 
    {
        //return wrappedTokens[_wNFTAddress][_wNFTTokenId];
        //ETypes.WNFT memory wnftOriginal = getWrappedToken(_wNFTAddress, _wNFTTokenId);
        wnft = getWrappedToken(_wNFTAddress, _wNFTTokenId);
        BlurredLiq memory liq = blurred[_wNFTAddress][_wNFTTokenId]; 
        if (keccak256(abi.encodePacked(_liquidityAsset, _secretHash)) == liq.hashedAsset)
        {
            ETypes.AssetItem[] memory liquidityItems = new ETypes.AssetItem[](1);
            liquidityItems[0] = ETypes.AssetItem(
                ETypes.Asset(ETypes.AssetType.ERC20, _liquidityAsset),
                0,
                _getBlurredAmount(liq)
            ); 
            wnft.collateral = liquidityItems;
        }
    }

    function getLiquidityProof(
        address _wNFTAddress, 
        uint256 _wNFTTokenId,
        address _liquidityAsset,
        address[] memory _sources,
        bytes32 _secretHash
    ) 
        public 
        view 
        returns (uint256 proofedAmount) 
    {
        BlurredLiq memory liq = blurred[_wNFTAddress][_wNFTTokenId];
        if (keccak256(abi.encodePacked(_liquidityAsset, _secretHash)) == liq.hashedAsset){
            proofedAmount = _getProofedBlurredAmount(
                liq, 
                _liquidityAsset,
                _sources, 
                _secretHash
            );
        } else {
            proofedAmount = 0;
        }
        
    }

    function getLiquidity(
        address _wNFTAddress, 
        uint256 _wNFTTokenId
    ) 
       public
       view
       returns (BlurredLiq memory)
    {
        return blurred[_wNFTAddress][_wNFTTokenId]; 
    }



    function hlpGetHashedSecret(uint256 _secret) public pure returns(bytes32) {
        return keccak256(abi.encode(_secret));
    }

    function hlpGetHashedAddress(address _addr) public pure returns(bytes32) {
        return keccak256(abi.encode(_addr));
    }

    function hlpGetBytes32Hashed(bytes32 _hash) public pure returns(bytes32) {
        return keccak256(abi.encode(_hash));
    }

    function hlpUintToBytes32(uint256 _secret) public pure returns(bytes32) {
        return bytes32(_secret);
    }
     
    function hlpBytes32ToUint(bytes32 _secret) public pure returns(uint256) {
        return uint256(_secret);
    }
    
    function hlpAssetAndSecretHash(address _asset, bytes32 _secretHash) public pure returns(bytes32) {
        return keccak256(abi.encodePacked(_asset, _secretHash));
    }

    /////////////////////////////////////////////////////////////////////
    //                    Internals                                    //
    /////////////////////////////////////////////////////////////////////
    function _addCollateral(
        address _wNFTAddress, 
        uint256 _wNFTTokenId, 
        ETypes.AssetItem[] calldata _collateral
    ) internal override 
    {}

    function _saveWNFTinfo(
        address wNFTAddress, 
        uint256 tokenId, 
        ETypes.INData calldata _inData
    ) internal override 
    {
        wrappedTokens[wNFTAddress][tokenId].inAsset = _inData.inAsset;
        // We will use _inData.unWrapDestination  ONLY for RENT implementation
        // wrappedTokens[wNFTAddress][tokenId].unWrapDestination = _inData.unWrapDestination;
        wrappedTokens[wNFTAddress][tokenId].unWrapDestination = address(0);
        wrappedTokens[wNFTAddress][tokenId].rules = _inData.rules;
        
        // Copying of type struct ETypes.Fee memory[] 
        // memory to storage not yet supported.
        // for (uint256 i = 0; i < _inData.fees.length; i ++) {
        //     wrappedTokens[wNFTAddress][tokenId].fees.push(_inData.fees[i]);            
        // }

        for (uint256 i = 0; i < _inData.locks.length; i ++) {
            wrappedTokens[wNFTAddress][tokenId].locks.push(_inData.locks[i]);            
        }

        // for (uint256 i = 0; i < _inData.royalties.length; i ++) {
        //     wrappedTokens[wNFTAddress][tokenId].royalties.push(_inData.royalties[i]);            
        // }

    }

    function _updateCollateralInfo(
        address _wNFTAddress, 
        uint256 _wNFTTokenId, 
        ETypes.AssetItem memory collateralItem
    ) internal override 
    {}

    function _newCollateralItem(
        address _wNFTAddress, 
        uint256 _wNFTTokenId, 
        ETypes.AssetItem memory collateralItem
    ) internal override 
    {}

    function _beforeUnWrapHook(
        address _wNFTAddress, 
        uint256 _wNFTTokenId, 
        bool _emergency
    ) internal override returns (bool)
    {}

    function _getBlurredAmount(BlurredLiq memory _liq)
        internal 
        view 
        returns(uint256 amount) 
    {
        for (uint256 i = 0; i < _liq.assetShares.length; i ++) {
             amount += _liq.assetShares[i].amount;
        }
    }

    function _getProofedBlurredAmount(
        BlurredLiq memory _liq, 
        address _liquidityAsset,
        address[] memory _sources, 
        bytes32 _secretHash
    )
        internal
        view
        returns(uint256 proofedAmount)
    {
        require(_liq.assetShares.length == _sources.length, "Arrays length must be equal");
        for (uint256 i = 0; i < _liq.assetShares.length; i ++) {
            if (
                keccak256(abi.encodePacked(_sources[i], _secretHash)) 
                == _liq.assetShares[i].hashedSource
            ) 
            {
                proofedAmount += IERC20Extended(_liquidityAsset).balanceOf(_sources[i]);
            } 
        }
        
    }

}