
# CHANGELOG

All notable changes to this project are documented in this file.

This changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [Unreleased]
- All crosschain
## [2.0.0](https://github.com/dao-envelop/envelop-protocol-v1/tree/1.2.0) - 2023-
### Added
- Fresh 
- Upgrade solidity version up to 0.8.19
- Open Zeppelin dependencies upgrade to 4.9.2

### Fixed
- Update unit tests and some deployment scripts
### Deprecated
- Some contracts are marked as depricated


