// SPDX-License-Identifier: MIT

pragma solidity 0.8.19;

import "@envelop-protocol-v1/interfaces/IWrapper.sol";

interface ITrustedWrapperEncoder is IWrapper  {

    function trustedOperator(address _operator) external view returns(bool);    
    
    function wrapEncoded(
        ETypes.INData calldata _inData, 
        ETypes.AssetItem[] calldata _collateral, 
        address _wrappFor
    ) 
        external
        payable
        returns (ETypes.AssetItem memory); 

   
}